#define BLANCO 0 //ASIGNACION DEL VALOR 0 A LA SUPERFICIE BLANCA
#define NEGRO 1 //ASIGNACION DEL VALOR 1 A LA SUPERFICIE NEGRA

byte sensorDerecho = 13; //Pin digital donde ser conectado el sensor derecho
byte sensorIzquierdo = 12; //Pin digital donde ser conectado el sensor izquierdo
byte motorDerechoA = 10; //Pin digital donde ser conectado una de las conexiones del motor derecho
byte motorDerechoB = 11; //Pin digital donde ser conectado una de las conexiones del motor derecho
byte motorIzquierdoA = 9;  //Pin digital donde ser conectado una de las conexiones del motor izquierdo
byte motorIzquierdoB = 6;  //Pin digital donde ser conectado una de las conexiones del motor izquierdo
int ValorSensorDerecho = 0; //Variable para almacenar el estado del sensor derecho
int ValorSensorIzquierdo = 0; //Variable para almacenar el estado del sensor izquierdo

//FUNCION PRINCIPAL DE CONFIGURACION
void setup (){
    
  Serial.begin (9600); //CONFIGURACION DE LA VELOCIDAD DE TRANSFERENCIA DE INFORMACION POR PUERTO SERIAL
  pinMode(sensorDerecho, INPUT); //CONFIGURACION DEL PIN DIGITAL COMO ENTRADA DE INFORMACION
  pinMode(sensorIzquierdo, INPUT); //CONFIGURACION DEL PIN DIGITAL COMO ENTRADA DE INFORMACION
  pinMode(motorDerechoA, OUTPUT);  //CONFIGURACION DEL PIN DIGITAL COMO SALIDA DE INFORMACION
  pinMode(motorDerechoB, OUTPUT);  //CONFIGURACION DEL PIN DIGITAL COMO SALIDA DE INFORMACION
  pinMode(motorIzquierdoA, OUTPUT); //CONFIGURACION DEL PIN DIGITAL COMO SALIDA DE INFORMACION
  pinMode(motorIzquierdoB, OUTPUT); //CONFIGURACION DEL PIN DIGITAL COMO SALIDA DE INFORMACION
  
}

//FUNCION PRINCIPAL DE EJECUCION CICLICA
void loop (){

ValorSensorDerecho = digitalRead(sensorDerecho); //ALMACENAMIENTO DEL VALOR ENVIADO POR EL SENSOR EN UNA VARIABLE
ValorSensorIzquierdo = digitalRead(sensorIzquierdo); //ALMACENAMIENTO DEL VALOR ENVIADO POR EL SENSOR EN UNA VARIABLE


//ADELANTE
if ((ValorSensorDerecho == BLANCO) && (ValorSensorIzquierdo == BLANCO)){ //ESTRUCTURA DE DECISION PARA ACTIVAR LOS MOTORES 

    digitalWrite(motorDerechoA, HIGH); //ACTIVACION DEL PIN PARA CONFIGURAR GIRO
    digitalWrite(motorDerechoB, LOW);  //DESACTIVACION DEL PIN PARA CONFIGURAR GIRO
    digitalWrite(motorIzquierdoA, HIGH);
    digitalWrite(motorIzquierdoB, LOW);
    
}
//DESVIO A LA IZQUIERDA
else if ((ValorSensorDerecho == BLANCO) && (ValorSensorIzquierdo == NEGRO)){
    
    digitalWrite(motorDerechoA, HIGH);
    digitalWrite(motorDerechoB, LOW);
    digitalWrite(motorIzquierdoA, LOW);
    digitalWrite(motorIzquierdoB, HIGH);
    
}
//DESVIO A LA DERECHA
else if ((ValorSensorDerecho == NEGRO) && (ValorSensorIzquierdo == BLANCO)){
    
    digitalWrite(motorDerechoA, LOW);
    digitalWrite(motorDerechoB, HIGH);
    digitalWrite(motorIzquierdoA, HIGH);
    digitalWrite(motorIzquierdoB, LOW);
    
}
//DETENIDO
else
{
    digitalWrite(motorDerechoA, LOW);
    digitalWrite(motorDerechoB, LOW);
    digitalWrite(motorIzquierdoA, LOW);
    digitalWrite(motorIzquierdoB, LOW);

}


Serial.print("Sensor Derecho: "); //MUESTRA EN PANTALLA EL MENSAJE
Serial.println(ValorSensorDerecho); //MUESTRA EN PANTALLA EL VALOR ACTUAL DEL SENSOR DERECHO
Serial.print("Sensor Izquierdo: ");
Serial.println(ValorSensorIzquierdo);

delay(300); //RETARDO PARA EVITAR LA SATURACION DE LA INFORMACION




}//FIN DE LA FUNCION CICLICA

